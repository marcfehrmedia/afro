import { FontAwesome } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';

import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import MapScreen from '../screens/MapScreen';
import ListScreen from '../screens/ListScreen';
import DownloadScreen from '../screens/DownloadScreen';
import ContributeScreen from '../screens/ContributeScreen';
import { BottomTabParamList, TabOneParamList, TabTwoParamList } from '../types';

const BottomTab = createBottomTabNavigator<BottomTabParamList>();

export default function BottomTabNavigator() {
  const colorScheme = useColorScheme();

  // @ts-ignore
    return (
    <BottomTab.Navigator
      initialRouteName="Map"
      tabBarOptions={{ activeTintColor: Colors[colorScheme].tint }}>
        <BottomTab.Screen
            name="Map"
            component={MapScreen}
            options={{
              tabBarIcon: ({ color }) => <TabBarIcon name="map" color={color} />,
            }}
        />
        <BottomTab.Screen
            name="List"
            component={ListScreen}
            options={{
              tabBarIcon: ({ color }) => <TabBarIcon name="list" color={color} />,
            }}
        />
        <BottomTab.Screen
            name="Download"
            component={DownloadScreen}
            options={{
             tabBarIcon: ({ color }) => <TabBarIcon name="download" color={color} />,
            }}
        />
        <BottomTab.Screen
            name="Contribute"
            component={ContributeScreen}
            options={{
                tabBarIcon: ({ color }) => <TabBarIcon name="photo" color={color} />,
            }}
        />
    </BottomTab.Navigator>
  );
}

// You can explore the built-in icon families and icons on the web at:
// https://icons.expo.fyi/
function TabBarIcon(props: { name: string; color: string }) {
  return <FontAwesome size={30} style={{ marginBottom: -3 }} {...props} />;
}

// Each tab has its own navigation stack, you can read more about this pattern here:
// https://reactnavigation.org/docs/tab-based-navigation#a-stack-navigator-for-each-tab
const TabOneStack = createStackNavigator<TabOneParamList>();

function TabOneNavigator() {
  return (
    <TabOneStack.Navigator>
      <TabOneStack.Screen
        name="TabOneScreen"
        component={TabOneScreen}
        options={{ headerTitle: 'Tab One Title' }}
      />
    </TabOneStack.Navigator>
  );
}

const TabTwoStack = createStackNavigator<TabTwoParamList>();

function TabTwoNavigator() {
  return (
    <TabTwoStack.Navigator>
      <TabTwoStack.Screen
        name="TabTwoScreen"
        component={TabTwoScreen}
        options={{ headerTitle: 'Tab Two Title' }}
      />
    </TabTwoStack.Navigator>
  );
}
