import React, { useState, useEffect } from 'react';
import {Button, StyleSheet, View, Dimensions, TouchableWithoutFeedback, ActivityIndicator} from 'react-native';
import Image from 'react-native-scalable-image';
// import { Button, View } from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import { Svg, Polyline } from 'react-native-svg';

const ScaledImage = (props: { uri: any; width: number | ((prevState: undefined) => undefined) | undefined; height: number | ((prevState: undefined) => undefined) | undefined; }) => {

    const [width, setWidth] = useState()
    const [height, setHeight] = useState()
    const [imageLoading, setImageLoading] = useState(true)

    useEffect(() => {
        // @ts-ignore
        Image.getSize(props.uri, (width1: number | ((prevState: undefined) => undefined) | undefined, height1: number | ((prevState: undefined) => undefined) | undefined) => {
            if (props.width && !props.height) {
                // @ts-ignore
                setWidth(props.width)
                // @ts-ignore
                setHeight(height1 * (props.width / width1))
            } else if (!props.width && props.height) {
                // @ts-ignore
                setWidth(width1 * (props.height / height1))
                // @ts-ignore
                setHeight(props.height)
            } else {
                // @ts-ignore
                setWidth(width1)
                // @ts-ignore
                setHeight(height1)
            }
            setImageLoading(false)
        }, (error: any) => {
            console.log("ScaledImage,Image.getSize failed with error: ", error)
        })
    }, [])


    return (
        height ?
            <View style={{ height: height, width: width, borderRadius: 5, backgroundColor: "lightgray" }}>
                <Image
                    source={{ uri: props.uri }}
                    style={{ height: height, width: width, borderRadius: 5, }}
                />
            </View>
            : imageLoading ?
            <ActivityIndicator size="large" />
            : null
    );
}

export default class ImagePickerComponent extends React.Component {
    state = {
        image: null,
        polyPoints: []
    };

    // @ts-ignore
    addPolyPoint(e) {
        console.log(e);
        const windowWidth = Dimensions.get('window').width;
        const relativePosX = e.nativeEvent.locationX / windowWidth * 100
        const relativePosY = e.nativeEvent.locationY / (windowWidth / 4 * 3) * 100
        const newPoint = ' ' + relativePosX + ',' + relativePosY
        this.setState({
            polyPoints: this.state.polyPoints + newPoint
        })
    }

    render() {
        let { image } = this.state;

        // @ts-ignore
        // @ts-ignore
        // @ts-ignore
        // @ts-ignore
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Button title="Pick an image from camera roll" onPress={this._pickImage} />
                {image &&
                <TouchableWithoutFeedback
	                onPress={(e) => this.addPolyPoint(e)}
                >
                    <View
                      style={styles.container}
                      width={Dimensions.get('window').width}
                      height={Dimensions.get('window').width / 4 * 3}
                    >
                        <Image
                            width={Dimensions.get('window').width}
                            source={{uri: image}}
                        />
                        <Svg
                          style={styles.svg}
                          width={Dimensions.get('window').width}
                          height={Dimensions.get('window').width / 4 * 3}
                          viewBox="0 0 100 100"
                          preserveAspectRatio="none"
                        >
                            <Polyline
                                points={this.state.polyPoints.length > 0 ? this.state.polyPoints : '0,0 0,0'}
                                fill="none"
                                stroke="white"
                                strokeWidth="1"
                                strokeLinecap={"round"}
                            />
                        </Svg>
                    </View>
                </TouchableWithoutFeedback>
                }
            </View>
        );
    }

    componentDidMount() {
        this.getPermissionAsync();
    }

    getPermissionAsync = async () => {
        // @ts-ignore
        if (Constants.platform.ios) {
            const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
            if (status !== 'granted') {
                alert('Sorry, we need camera roll permissions to make this work!');
            }
        }
    };

    _pickImage = async () => {
        try {
            let result = await ImagePicker.launchImageLibraryAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.All,
                allowsEditing: false,
                aspect: [4, 3],
                quality: 1,
            });
            if (!result.cancelled) {
                this.setState({ image: result.uri });
            }

            console.log(result);
        } catch (E) {
            console.log(E);
        }
    };
}

const styles = StyleSheet.create({
    container: {
        position: 'relative',
        overflow: 'hidden'
    },
    svg: {
        position: 'absolute',
        top: 0,
        left: 0,
        overflow: 'hidden',
        width: '100%',
        height: '100%'
    }
});