import React, { Component } from 'react';
import {Image, View, Text, Dimensions, ScrollView} from 'react-native';
import {Grid, Col, Row} from 'react-native-easy-grid';
import SunCalc from 'suncalc';
// import SunCalc from '../scripts/suncalc.js';
import moment from 'moment-timezone';
import { debounce } from 'underscore';
import { Magnetometer } from 'expo-sensors';
// import { ScreenOrientation} from 'expo';

/* Import store */
import Store from '../store/index';
import { observer } from 'mobx-react';

const {height, width} = Dimensions.get('window');

@observer
export default class SunCalculationComponent extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this._toggle();
    };

    UNSAFE_componentWillMount() {
        this._sunCalculations();
    }

    componentDidUpdate(prevProps) {
        if(prevProps.lat !== this.props.lat || prevProps.lon !== this.props.lon) {
            this._sunCalculations();
        }
    }

    componentWillUnmount() {
        this._unsubscribe();
    };

    _sunCalculations = () => {
        let lat = Store.currentPosition.latitude;
        let lon = Store.currentPosition.longitude;
        const dateNow = new Date().getTime();
        const currentTime = {
            capetown: moment.tz(dateNow, "Africa/Windhoek")
        };
        Store.setCurrentTime(currentTime);
        const sunCalculations = SunCalc.getTimes(currentTime.capetown, lat, lon);
        const sunPosition = SunCalc.getPosition(currentTime.capetown, lat, lon);
        const sunAngle = sunPosition.altitude * 180 / Math.PI;
        const sunDirection = sunPosition.azimuth / Math.PI * 180 + 180;
        // const moonCalculations = SunCalc.getTimes(currentTime.capetown, lat, lon);
        // const moonPosition = SunCalc.getMoonPosition(currentTime.capetown, lat, lon);
        // const moonAngle = moonPosition.altitude * 180 / Math.PI;
        // const moonDirection = moonPosition.azimuth / Math.PI * 180 + 180;
        // console.log('sunDirection: ' + sunDirection);
        // console.log('moonDirection: ' + moonDirection);
        Store.setSunCalculations(sunCalculations);
        Store.setSunPosition(sunAngle, sunDirection);
        // Store.setMoonCalculations(moonCalculations);
        // Store.setMoonPosition(moonAngle, moonDirection);
    };

    _toggle = () => {
        if (this._subscription) {
            this._unsubscribe();
        } else {
            this._subscribe();
        }
    };

    _subscribe = async () => {
        this._subscription = Magnetometer.addListener((data) => {
            debounce(Store.setCompassPosition(this._angle(data)), 250, false)
        });
    };

    _unsubscribe = () => {
        this._subscription && this._subscription.remove();
        this._subscription = null;
    };

    _angle = (magnetometer) => {
        if (magnetometer) {
            let {x, y, z} = magnetometer;
            if (Math.atan2(y, x) >= 0) {
                angle = Math.atan2(y, x) * (180 / Math.PI);
            }
            else {
                angle = (Math.atan2(y, x) + 2 * Math.PI) * (180 / Math.PI);
            }
        }
        return Math.round(angle);
    };

    _direction = (degree) => {
        if (degree >= 22.5 && degree < 67.5) {
            return 'NE';
        }
        else if (degree >= 67.5 && degree < 112.5) {
            return 'E';
        }
        else if (degree >= 112.5 && degree < 157.5) {
            return 'SE';
        }
        else if (degree >= 157.5 && degree < 202.5) {
            return 'S';
        }
        else if (degree >= 202.5 && degree < 247.5) {
            return 'SW';
        }
        else if (degree >= 247.5 && degree < 292.5) {
            return 'W';
        }
        else if (degree >= 292.5 && degree < 337.5) {
            return 'NW';
        }
        else {
            return 'N';
        }
    };

    // Match the device top with pointer 0° degree. (By default 0° starts from the right of the device.)
    _degree = (magnetometer) => {
        return magnetometer - 90 >= 0 ? magnetometer - 90 : magnetometer + 271;
    };

    render() {
        const compassDegreeInt = parseInt(this._degree(Store.compassPosition));

        let compassImageSource = require('../assets/icons/compass-grey.png');
        let degreeColor = '#282828';

        if (compassDegreeInt >= 270 || compassDegreeInt <= 90) {
            compassImageSource = require('../assets/icons/compass-red.png');
            degreeColor = '#F3295C';
        }

        if (compassDegreeInt <= 5 || compassDegreeInt >= 355) {
            compassImageSource = require('../assets/icons/compass-green.png');
            degreeColor = '#39ca74';
        }

        return (
            <View pointerEvents="none"
                  style={{position: 'absolute', flex: 1, top: 20, left: 0, zIndex: 99, width: '100%', height: '100%', justifyContent: 'flex-end'}}
            >
                <View style={{alignSelf: 'center', backgroundColor: 'rgba(0,0,0,0.5)', padding: 5, width: '100%'}}>
                    {Store.sunCalculations &&
										<Text style={{fontSize: 15, textAlign: 'center', color: '#fff'}}>
											Sunrise: {moment(Store.sunCalculations.sunrise).format('LTS')} Sunset: {moment(Store.sunCalculations.sunset).format('LTS')}
										</Text>
                    }
                    {Store.moonCalculations &&
										<Text style={{fontSize: 12, textAlign: 'center', color: '#fff'}}>
											☼ {Math.round(Store.sun.direction)}deg {Math.round(Store.sun.angle)}°  ☾ {Math.round(Store.moon.direction)}deg {Math.round(Store.moon.angle)}°
										</Text>
                    }
                </View>
                <Grid style={{backgroundColor: 'transparent'}}>
                    <Row style={{alignItems: 'center'}} size={0.1}>
                        {/*
                        <View style={{position: 'absolute', width: width, alignItems: 'center', top: 0}}>
                            <Image source={compassImageSource} style={{
                                height: height / 20,
                                resizeMode: 'contain'
                            }}/>
                        </View>
                        */}
                        <Text style={{
                            color: degreeColor,
                            fontSize: 16,
                            fontWeight: 'bold',
                            width: width,
                            position: 'absolute',
                            textAlign: 'center'
                        }}>
                            {this._direction(this._degree(Store.compassPosition))} ↑ {Math.ceil(this._degree(Store.compassPosition)/5)*5}°
                        </Text>
                    </Row>
                </Grid>

            </View>
        );
    }
}