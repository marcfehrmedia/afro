import React, { Component } from 'react';
import { Marker } from 'react-native-maps';
import { Image } from 'react-native';

/* Import store */
import Store from '../store/index';
import { observer } from 'mobx-react';

@observer
export default class SunMarkerComponent extends Component {
    constructor(props) {
        super(props);
        this._sunMoonIconPos = this._sunMoonIconPos.bind(this);
        this._degrees_to_radians = this._degrees_to_radians.bind(this);
    }

    _degrees_to_radians(degrees) {
        const pi = Math.PI;
        return degrees * (pi / 180);
    }

    _sunMoonIconPos() {
        const mapMargin = 3;
        const sunDirectionRadians = this._degrees_to_radians(Store.sun.direction);
        // const moonDirectionRadians = this._degrees_to_radians(Store.moon.direction);
        const e = Store.mapRegion.latitudeDelta > Store.mapRegion.longitudeDelta ? Store.mapRegion.longitudeDelta / mapMargin : Store.mapRegion.latitudeDelta / mapMargin;
        const sunLat = this.props.coords.latitude + (e * Math.cos(sunDirectionRadians));
        // const moonLat = this.props.coords.latitude + (e * Math.cos(moonDirectionRadians));
        const sunLon = this.props.coords.longitude + (e * Math.sin(sunDirectionRadians));
        // const moonLon = this.props.coords.longitude + (e * Math.sin(moonDirectionRadians));
        return {sunLat, sunLon};
    }

    render() {
        const { sunLat, sunLon } = this._sunMoonIconPos();
        // console.log(Store.sun.direction)

        return (
            <Marker
                coordinate={{
                    latitude: sunLat,
                    longitude: sunLon
                }}
                title={"The sun"}
                description={"The sun in relation to your current position."}
            >
                <Image
                    source={require('../assets/icons/sun.png')}
                    style={{ width: 50, height: 50 }}
                    imageResizeMode={'cover'}
                />
            </Marker>
        );
    }
}