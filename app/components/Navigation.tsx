import * as React from 'react';
import { Button, View, Text } from 'react-native';

function NavigationComponent(props) {
    return (
        <Button
            title="Go to Details... again"
            onPress={() => props.navigation.push('Details')}
        />
    );
}

export default NavigationComponent;
