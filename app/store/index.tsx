import { observable, action } from 'mobx'

class GlobalStateStore{

    @observable mapOptions = {
        permissions: {
            location: false,
            error: null
        },
        layers: {
            slingsby: true,
        },
        visible: false,
        backgroundImageUrl: null
    };
    @observable sun = {
        angle: 0,
        direction: 0,
    };
    @observable moon = {
        angle: 0,
        direction: 0,
    };
    @observable currentPosition = {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0,
        longitudeDelta: 0,
    };
    @observable mapRegion = {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0,
        longitudeDelta: 0,
    };
    @observable compassPosition = 0;
    @observable currentTime = {};
    @observable sunCalculations = {};
    @observable moonCalculations = {};

    @action setBackgroundImageUri (uri) {
        this.mapOptions.backgroundImageUrl = uri;
    }
    @action setSunPosition (angle, direction) {
        this.sun = {
            angle,
            direction
        }
    }
    @action setMoonPosition (angle, direction) {
        this.moon = {
            angle,
            direction
        }
    }
    @action setCompassPosition (deg) {
        this.compassPosition = deg
    }
    @action setSunCalculations (sunCalculations) {
        this.sunCalculations = sunCalculations
    }
    @action setMoonCalculations (moonCalculations) {
        this.moonCalculations = moonCalculations
    }
    @action setMapRegion (mapRegion) {
        this.mapRegion = mapRegion
    }
    @action setCurrentPosition (currentPosition) {
        this.currentPosition = {
            latitude: currentPosition.latitude,
            longitude: currentPosition.longitude,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421
        }
    }
    @action setCurrentTime (currentTime) {
        this.currentTime = currentTime
    }
    @action toggleSlingsbyLayer () {
        this.mapOptions.layers.slingsby = !this.mapOptions.layers.slingsby
    }
    @action toggleMap () {
        this.mapOptions.visible = !this.mapOptions.visible;
    }
    @action showMap () {
        this.mapOptions.visible = true
    }
    @action setLocationPermission (bool) {
        this.mapOptions.permissions.location = bool;
    }
    @action setLocationResultError (message) {
        this.mapOptions.permissions.error = message;
    }
}

const store = new GlobalStateStore();
export default store