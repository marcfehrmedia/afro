import * as React from 'react';
import { StyleSheet } from 'react-native';

import NavigationComponent from '../components/Navigation';
import ImagePickerComponent from '../components/ImagePicker';

import { Text, View } from '../components/Themed';

export default function ContributeScreen({ navigation }) {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Add a new line</Text>
      <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
      <ImagePickerComponent />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 50
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
