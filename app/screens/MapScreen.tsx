import React, { useState, useEffect } from "react";
import { StyleSheet, Image } from 'react-native';
import MapView from "react-native-map-clustering";
import { Marker, UrlTile, LocalTile } from 'react-native-maps';
import * as Location from 'expo-location';
import SunCalculationComponent  from '../components/Suncalc';
import SunMarkerComponent  from '../components/SunMarker';

import EditScreenInfo from '../components/EditScreenInfo';
import { Text, View } from '../components/Themed';

export default function MapScreen() {
  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);
  const [mapBaseTiles, setMapBaseTiles] = useState('http://tile.thunderforest.com/outdoors/{z}/{x}/{y}.png?apikey=75896d4e543b4a739f44c1f231806c69');
  const [slingsbyMap, setSlingsbyMap] = useState(true)

  useEffect(() => {
    (async () => {
      let { status } = await Location.requestPermissionsAsync();
      if (status !== 'granted') {
        setErrorMsg('Permission to access location was denied');
      }

      let location = await Location.getCurrentPositionAsync({});
      // console.log(location.coords)
      setLocation(location);
    })();
  });

  let text = 'Waiting..';
  if (errorMsg) {
    text = errorMsg;
  } else if (location) {
    text = JSON.stringify(location);
  }

  const _degrees_to_radians = (degrees) => {
    const pi = Math.PI;
    return degrees * (pi / 180);
  }

  return (
      <View style={styles.container}>

        {location &&
        <SunCalculationComponent
            lat={location.coords.latitude}
            lon={location.coords.longitude}
        />
        }

        {location &&
        <MapView
            style={ styles.map }
            showsUserLocation
            initialRegion={{
              latitude: location.coords.latitude,
              longitude: location.coords.longitude,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}
            clusterColor={'#F52E67'}
            spiderLineColor={'transparent'}
        >
          <UrlTile
              urlTemplate={mapBaseTiles}
              zIndex={-1}
              maximumZ={18}
              tileSize={256}
          />
          {slingsbyMap &&
          <UrlTile
              urlTemplate={`http://www.marcfehr.ch/afro/{z}/{x}/{y}.png`}
              zIndex={-1}
              maximumZ={18}
              tileSize={256}
          />
          }
          <Marker
              coordinate={{latitude: -32.120618, longitude: 19.059562,}}
              title={"De Pakhuis"}
              description={"A lot of Wusel here."}
              onPress={() => console.log('testing the keypress event')}
          >
            <Image
                source={require('../assets/icons/boulder.png')}
                style={{ width: 25, height: 25 }}
                imageResizeMode={'cover'}
            />
          </Marker>
          <Marker
              coordinate={{latitude: -32.120625, longitude: 19.059544,}}
              title={"De Pakhuis"}
              description={"A lot of Wusel here."}
              onPress={() => console.log('testing the keypress event')}
          >
            <Image
                source={require('../assets/icons/boulder.png')}
                style={{ width: 25, height: 25 }}
                imageResizeMode={'cover'}
            />
          </Marker>
          <Marker
              coordinate={{latitude: -32.120622, longitude: 19.059582,}}
              title={"De Pakhuis"}
              description={"A lot of Wusel here."}
              onPress={() => console.log('testing the keypress event')}
          >
            <Image
                source={require('../assets/icons/boulder.png')}
                style={{ width: 25, height: 25 }}
                imageResizeMode={'cover'}
            />
          </Marker>
          <Marker
              coordinate={{latitude: -32.120620, longitude: 19.059572,}}
              title={"De Pakhuis"}
              description={"A lot of Wusel here."}
              onPress={() => console.log('testing the keypress event')}
          >
            <Image
                source={require('../assets/icons/boulder.png')}
                style={{ width: 25, height: 25 }}
                imageResizeMode={'cover'}
            />
          </Marker>
          <SunMarkerComponent
              coords={location.coords}
          />
        </MapView>
        }

      </View>
  )
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  status: {
    position: 'absolute',
    top: 20,
    zIndex: 999
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
});
