# AFRO | App for Rocklands

<img width="300px" src="https://scontent.fcpt4-1.fna.fbcdn.net/v/t1.0-9/117708436_10162035877058084_225090516487381896_o.jpg?_nc_cat=100&_nc_sid=dd9801&_nc_eui2=AeGQ3WxoNmylnfkLJpk1LJNvNlhJZi4yIeE2WElmLjIh4W74eir2LicFtDvAQHMw0nM&_nc_ohc=AeN1VF4Z58cAX88iUdP&_nc_ht=scontent.fcpt4-1.fna&oh=2acfd4c3f36c92116a4efa0b44952882&oe=5F593433" title="Rocklands" alt="Sky above Rocklands">

An open source project for the climbing community.

> Built with React Native and EXPO for both iOS and Android devices

> Based on an open source API

> Free for anyone

- Make sure you use Node Version Manager (nvm) and install with the right version of Node (14.8.0) as defined in .nvmrc (terminal command nvm use in main folder)
- App content in ./app
– Feedback about this repository very welcome: marc.fehr@gmail.com

> Goals for the app

- Have an intuitive, accurate and usable map to discover climbing areas in South Africa
- Create an open source database with a solid API to start collecting data
- Make adding new climbs easy and fun – we all need something fun to do on our rest days
– The app must be usable offline, without having any mobile data
– For the map we will try to use different layers, some of them may in future have to be paid for

---

## Installation

First, clone (or fork) the repository, open it in an editor, and...

1. `cd ./app`
2. `nvm use`
3. `yarn install`
4 `yarn start`

### Clone

- Clone this repo to your local machine using `git@gitlab.com:marcfehrmedia/afro.git` or `https://gitlab.com/marcfehrmedia/afro.git`

## Team

– Marc Fehr, marc.fehr@gmail.com

---

## Support

Reach out to me at one of the following places!

- Website at <a href="https://www.marcfehr.ch" target="_blank">`marcfehr.ch`</a>
- Twitter at <a href="https://twitter.com/mrcfhr" target="_blank">`@mrcfhr`</a>
- Facebook at <a href="https://www.facebook.com/marc.fehr" target="_blank">Marc Fehr</a>

---

## Donations (Optional)

[![Buy me a coffee](https://cdn.buymeacoffee.com/buttons/default-orange.png)](https://www.buymeacoff.ee/marcfehr)


---

## License

[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

- **[MIT license](http://opensource.org/licenses/mit-license.php)**
- © Copyright 2020 <a href="https://www.marcfehr.ch" target="_blank">Marc Fehr Media</a>.